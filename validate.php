<?php 


function isOldEnough($year,$month,$day){

$required = 21; // age you are checking


		//date in mm/dd/yyyy format; or it can be in other formats as well
         $birthDate = "$month/$day/$year";
         //explode the date to get month, day and year
         $birthDate = explode("/", $birthDate);
         //get age from date or birthdate
         $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));
       
 if ($age > $required){
 	$oldEnough = true;
 }
 else {
 	$oldEnough = false;
 }

return $oldEnough;

};


//var_dump(isOldEnough(04,16,2011));